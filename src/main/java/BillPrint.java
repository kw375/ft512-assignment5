import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 */
public class BillPrint {
    private final HashMap<String, Play> plays;
    private final String customer;
    private final ArrayList<Performance> performances;

    public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
        this.plays = new HashMap<>();
        for (Play p : plays) {
            this.plays.put(p.getId(), p);
        }
        this.customer = customer;
        this.performances = performances;
    }

    public HashMap<String, Play> getPlays() {
        return plays;
    }


    public String getCustomer() {
        return customer;
    }

    public ArrayList<Performance> getPerformances() {
        return performances;
    }

    public String statement() {
        DecimalFormat numberFormat = new DecimalFormat("#.00");
        StringBuilder result = new StringBuilder("Statement for " + this.customer + "\n");
        for (Performance perf : performances) {
            Play play = plays.get(perf.getPlayID());
            if (play == null) {
                throw new IllegalArgumentException("No play found");
            }
            result.append("  ").append(play.getName()).append(": $").append(numberFormat.format((double) play.getAmount(perf.getAudience()) / 100.00)).append(" (").append(perf.getAudience()).append(" seats)").append("\n");

        }
        result.append("Amount owed is $").append(numberFormat.format((double) getTotalAmount() / 100)).append("\n");
        result.append("You earned ").append(getTotalVolumeCredits()).append(" credits").append("\n");
        return result.toString();
    }

    private int getTotalAmount() {
        int totalAmount = 0;
        for (Performance perf : performances) {
            Play play = plays.get(perf.getPlayID());
            totalAmount += play.getAmount(perf.getAudience());
        }
        return totalAmount;
    }

    private int getTotalVolumeCredits() {
        int volumeCredits = 0;
        for (Performance perf : performances) {
            Play play = plays.get(perf.getPlayID());
            volumeCredits = perf.getVolumeCredits(volumeCredits, play.getType());
        }
        return volumeCredits;
    }


    public static void main(String[] args) {
        Play p1 = new Play("hamlet", "Hamlet", "tragedy");
        Play p2 = new Play("as-like", "As You Like It", "comedy");
        Play p3 = new Play("othello", "Othello", "tragedy");
        ArrayList<Play> pList = new ArrayList<Play>();
        pList.add(p1);
        pList.add(p2);
        pList.add(p3);
        Performance per1 = new Performance("hamlet", 55);
        Performance per2 = new Performance("as-like", 35);
        Performance per3 = new Performance("othello", 40);
        ArrayList<Performance> perList = new ArrayList<Performance>();
        perList.add(per1);
        perList.add(per2);
        perList.add(per3);
        String customer = "BigCo";
        BillPrint app = new BillPrint(pList, customer, perList);
        System.out.println(app.statement());
    }

}
