/**
 * @author Minglun Zhang
 */
public class Performance {
    private String playID;
    private int audience;

    public Performance(String playID, int audience) {
        this.playID = playID;
        this.audience = audience;
    }

    public String getPlayID() {
        return playID;
    }

    public void setPlayID(String playID) {
        this.playID = playID;
    }

    public int getAudience() {
        return audience;
    }

    public void setAudience(int audience) {
        this.audience = audience;
    }

    public int getVolumeCredits(int volumeCredits, String type) {
        volumeCredits += Math.max(this.getAudience() - 30, 0);
        if (type.equals("comedy")) {
            volumeCredits += Math.floor((double) this.getAudience() / 5.0);
        }
        return volumeCredits;
    }
}
