public class Amount {
    public Amount amountBuild(String type) {
        return switch (type) {
            case "tragedy" -> new TragedyAmount();
            case "comedy" -> new ComedyAmount();
            default -> throw new IllegalArgumentException("unknown type: " + type);
        };
    }

    public int getAmount(int audience) {
        return 0;
    }
}
